*********************************
:mod:`domdf_python_tools.dates`
*********************************

.. extras-require:: dates

	pytz >=2019.1

.. contents:: Table of Contents


.. warning:: This module has not been fully tested. Use with caution.


.. automodule:: domdf_python_tools.dates
	:members:
	:undoc-members:
