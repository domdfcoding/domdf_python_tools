=========================================
:mod:`domdf_python_tools.pagesizes`
=========================================

.. contents:: Table of Contents

.. automodule:: domdf_python_tools.pagesizes
	:members:
	:undoc-members:


:mod:`domdf_python_tools.pagesizes.classes`
----------------------------------------------

.. automodule:: domdf_python_tools.pagesizes.classes
	:members:
	:undoc-members:


:mod:`domdf_python_tools.pagesizes.sizes`
----------------------------------------------

.. automodule:: domdf_python_tools.pagesizes.sizes
	:members:
	:undoc-members:


:mod:`domdf_python_tools.pagesizes.units`
----------------------------------------------

.. automodule:: domdf_python_tools.pagesizes.units
	:members:
	:undoc-members:



:mod:`domdf_python_tools.pagesizes.utils`
----------------------------------------------

.. automodule:: domdf_python_tools.pagesizes.utils
	:members:
	:undoc-members:
