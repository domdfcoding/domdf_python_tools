*********************************
:mod:`domdf_python_tools.utils`
*********************************

.. contents:: Table of Contents

.. automodule:: domdf_python_tools.utils
	:members:
	:undoc-members:
