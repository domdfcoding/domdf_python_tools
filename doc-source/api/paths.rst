**********************************
:mod:`domdf_python_tools.paths`
**********************************

.. contents:: Table of Contents

.. automodule:: domdf_python_tools.paths
	:members:
	:undoc-members:
