*********************************
:mod:`domdf_python_tools.bases`
*********************************

.. contents:: Table of Contents

.. automodule:: domdf_python_tools.bases
	:members:
	:undoc-members:
