# stdlib
from decimal import Decimal
from typing import Union

AnyNumber = Union[float, int, Decimal]
